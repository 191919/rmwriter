#include "stdafx.h"
#include <InitGuid.h>
#include "RMWriterProp.h"
#include "IRMWriterFilter.h"
#include "CFilterRMWriter.h"
#include "CRMWriter.h"

class CSampleEventSink : public IHXTEventSink
{
public:
	CSampleEventSink() 
	{
		m_ulRefCount = 0;
		m_bEncodingStarted = FALSE;
		m_bEncodingFinished = FALSE;
	}

	//IUnknown methods
	STDMETHOD_(ULONG, AddRef)()
	{
		return InterlockedIncrement(&m_ulRefCount);
	}
	
	STDMETHOD_(ULONG, Release)()
	{
		if (InterlockedDecrement(&m_ulRefCount) > 0)
		{
			return m_ulRefCount;
		}

		delete this;
		return 0;
	}

	STDMETHOD(QueryInterface)(REFIID riid, void** ppvObj)
	{
		if (!ppvObj)
		{
			return HXR_POINTER;				
		}

		if (IsEqualIID(IID_IHXTEventSink, riid))			
		{							
			AddRef();					
			*ppvObj = (IHXTEventSink*)this;
			return HXR_OK;					
		}

		return HXR_FAIL;
	}

	// IHXTEventSink methods
	STDMETHOD(HandleEvent)(EHXTEvent eEvent, UINT32 *puValue, const char *cszValue, IUnknown *pUnknown)
	{
		switch (eEvent)
		{
		case eEventEncodingStarted:
			m_bEncodingStarted = TRUE;
			break;

		case eEventEncodingFinished:
			m_bEncodingFinished = TRUE;
			break;
		}

		return HXR_OK;
	}

private:
	UINT32 m_ulRefCount;

public:
	volatile BOOL m_bEncodingStarted;
	volatile BOOL m_bEncodingFinished;
};

////////////////////////////////////////////////////////////////////////////

CRMWriter::CRMWriter(CFilterRMWriter* pMainFilter, HRESULT* phr)
{
	m_pMainFilter = pMainFilter;
	m_wszDestFile[0] = L'\0';
	
	m_pFactory = NULL;
	m_pJob = NULL;
	m_pFileLogObserver = NULL;
	m_pLogSystem = NULL;
	m_pAudioPin = NULL;
	m_pVideoPin = NULL;
	m_pAllocator = NULL;
	m_pInput = NULL;

	m_pEventSink = NULL;

	m_rtAudioTime = 0;
	m_rtVideoTime = 0;

	m_dwVideoSampleSize = 0;

	memset(m_szDllPath, 0, sizeof(m_szDllPath));

	*phr = InitRMWriter();
}

CRMWriter::~CRMWriter()
{
}

HRESULT CRMWriter::SetDestFile(LPCOLESTR pFile)
{
#ifdef _DEBUG
	char szFile[MAX_PATH];
	WideCharToMultiByte(CP_ACP, 0, pFile, -1, szFile, MAX_PATH, 0, 0);
#endif // _DEBUG

	wcscpy(m_wszDestFile, pFile);
	return NOERROR;
}

HRESULT CRMWriter::GetDestFile(LPOLESTR * ppFile)
{
    *ppFile = (LPOLESTR) CoTaskMemAlloc(sizeof(WCHAR) * (1+wcslen(m_wszDestFile)));

    if (*ppFile != NULL) 
    {
		wcscpy(*ppFile, m_wszDestFile);
		return NOERROR;
	}
	return E_OUTOFMEMORY;
}

int DSVideoTypeToHXTFormat(const GUID& guidSubType, VIDEOINFOHEADER* pvih)
{
	if (guidSubType == MEDIASUBTYPE_RGB24)
	{
		return pvih->bmiHeader.biHeight >= 0 ? HXT_VIDEO_FORMAT_BGR24_INVERTED : HXT_VIDEO_FORMAT_BGR24_NONINVERTED;
	}
	else if (guidSubType == MEDIASUBTYPE_ARGB32)
	{
		return pvih->bmiHeader.biHeight >= 0 ? HXT_VIDEO_FORMAT_BGRA32_INVERTED : HXT_VIDEO_FORMAT_BGRA32_NONINVERTED;
	}
	else if (guidSubType == MEDIASUBTYPE_RGB555)
	{
		return pvih->bmiHeader.biHeight >= 0 ? HXT_VIDEO_FORMAT_LE_ARGB555_INVERTED : HXT_VIDEO_FORMAT_LE_ARGB555_NONINVERTED;
	}
	else if (guidSubType == MEDIASUBTYPE_RGB565)
	{
		return pvih->bmiHeader.biHeight >= 0 ? HXT_VIDEO_FORMAT_LE_RGB565_INVERTED :  HXT_VIDEO_FORMAT_LE_RGB565_NONINVERTED;
	}
	else if (guidSubType == MEDIASUBTYPE_RGB8)
	{
		return pvih->bmiHeader.biHeight >= 0 ? HXT_VIDEO_FORMAT_BGR8_INVERTED : HXT_VIDEO_FORMAT_BGR8_NONINVERTED;
	}
	else if (guidSubType == MEDIASUBTYPE_YV12)
	{
		return HXT_VIDEO_FORMAT_YV12;
	}
	else if (guidSubType == MEDIASUBTYPE_YVU9)
	{
		return HXT_VIDEO_FORMAT_YVU9;
	}
	else if (guidSubType == MEDIASUBTYPE_IF09)
	{
		return HXT_VIDEO_FORMAT_IF09;
	}
	else if (guidSubType == MEDIASUBTYPE_YUY2)
	{
		return pvih->bmiHeader.biHeight >= 0 ? HXT_VIDEO_FORMAT_YUY2 : HXT_VIDEO_FORMAT_YUY2_INVERTED;
	}
	else if (guidSubType == MEDIASUBTYPE_UYVY)
	{
		return HXT_VIDEO_FORMAT_UYVY;
	}
	else if (guidSubType == MEDIASUBTYPE_YVYU)
	{
		return HXT_VIDEO_FORMAT_YVYU;
	}
	else if (guidSubType == MEDIASUBTYPE_Y211)
	{
		return HXT_VIDEO_FORMAT_Y211;
	}
	return -1;
}

HRESULT CRMWriter::ConfigVideoInput(const GUID& guidSubType, VIDEOINFOHEADER * pvih)
{
	HX_RESULT res = HXR_OK;

	// Create the property bag used to initialize the input
	IHXTPropertyBag* pInitParams = NULL;
	if (SUCCEEDED(res))
		res = m_pFactory->CreateInstance(IID_IHXTPropertyBag, (IUnknown**)&pInitParams);

	// Set the plugin type
	if (SUCCEEDED(res))
		res = pInitParams->SetString(kPropPluginType, kValuePluginTypeInputMediaSink);

	// Get the video pin			
	IUnknown* pUnk = NULL;
	res = m_pInput->GetUnknown( kPropVideoInputPin, &pUnk );

	if (SUCCEEDED(res))
		res = pUnk->QueryInterface(IID_IHXTMediaInputPin, (void**)&m_pVideoPin);

	// Enable pin
	if (SUCCEEDED(res))
		res = m_pVideoPin->SetPinEnabled(TRUE);

	// Set video format
	IHXTVideoPinFormat* pPinFormat = NULL;
	if (SUCCEEDED(res))
	{
		res = pUnk->QueryInterface(IID_IHXTVideoPinFormat, (void**)&pPinFormat);
	}

	if (SUCCEEDED(res))
	{
		res = pPinFormat->SetFrameDimensions(pvih->bmiHeader.biWidth, abs(pvih->bmiHeader.biHeight));
		m_dwVideoSampleSize = pvih->bmiHeader.biWidth * abs(pvih->bmiHeader.biHeight) * sizeof(UINT32);
	}

	if (SUCCEEDED(res))
	{
		res = pPinFormat->SetColorFormat((EHXTVideoColorFormat) DSVideoTypeToHXTFormat(guidSubType, pvih));
	}

	if (SUCCEEDED(res))
	{
		res = pPinFormat->SetFrameRate(23.976);
	}

	HX_RELEASE(pPinFormat);
	HX_RELEASE(pUnk);

	HX_RELEASE(pInitParams);

	return S_OK;
}

HRESULT CRMWriter::ConfigAudioInput(const GUID& guidSubType, WAVEFORMATEX * pwf)
{
	HX_RESULT res = HXR_OK;

	// Create the property bag used to initialize the input
	IHXTPropertyBag* pInitParams = NULL;
	if (SUCCEEDED(res))
		res = m_pFactory->CreateInstance(IID_IHXTPropertyBag, (IUnknown**)&pInitParams);

	// Set the plugin type
	if (SUCCEEDED(res))
		res = pInitParams->SetString(kPropPluginType, kValuePluginTypeInputMediaSink);

	// Get the audio pin
	IUnknown* pUnk = NULL;
	res = m_pInput->GetUnknown( kPropAudioInputPin, &pUnk );

	if (SUCCEEDED(res))
		res = pUnk->QueryInterface(IID_IHXTMediaInputPin, (void**)&m_pAudioPin);

	// Enable pin
	if (SUCCEEDED(res))
		res = m_pAudioPin->SetPinEnabled(TRUE);

	// Set audio format -- see ihxtaudioformat.h for enum definitions
	IHXTAudioPinFormat* pPinFormat = NULL;
	if (SUCCEEDED(res))
		res = pUnk->QueryInterface(IID_IHXTAudioPinFormat, (void**)&pPinFormat);

	if (SUCCEEDED(res))
	{
		res = pPinFormat->SetSampleRate(pwf->nSamplesPerSec);
	}

	if (SUCCEEDED(res))
	{
		res = pPinFormat->SetSampleFormat(HXT_AUDIO_FORMAT_NE_16_2);
	}

	if (SUCCEEDED(res))
	{
		res = pPinFormat->SetChannelFormat(pwf->nChannels == 2 ? HXT_AUDIO_STEREO : HXT_AUDIO_MONO);
	}

	HX_RELEASE(pPinFormat);
	HX_RELEASE(pUnk);

	HX_RELEASE(pInitParams);

	return S_OK;
}

// Create writer object, obtain some interfaces and select a profile
HRESULT CRMWriter::InitRMWriter()
{
	UINT32 ulNumChars = 0;
	ulNumChars += wsprintf(m_szDllPath+ulNumChars, "%s=%s\\%s", "DT_Plugins", m_pMainFilter->m_params.szSDKPath, "plugins") + 1;
	ulNumChars += wsprintf(m_szDllPath+ulNumChars, "%s=%s\\%s", "DT_Codecs", m_pMainFilter->m_params.szSDKPath, "codecs") + 1;
	ulNumChars += wsprintf(m_szDllPath+ulNumChars, "%s=%s\\%s", "DT_EncSDK", m_pMainFilter->m_params.szSDKPath, "tools") + 1;
	ulNumChars += wsprintf(m_szDllPath+ulNumChars, "%s=%s\\%s", "DT_Common", m_pMainFilter->m_params.szSDKPath, "common") + 1;

	HX_RESULT res = HXR_OK;

	char szEncSessionDllPath[MAX_PATH];
	wsprintf(szEncSessionDllPath, "%s\\tools\\encsession.dll", m_pMainFilter->m_params.szSDKPath);

	m_RmsessionDLL = ::LoadLibrary(szEncSessionDllPath);
	if (m_RmsessionDLL)
	{
		FPRMBUILDSETDLLACCESSPATH fpSetDllAccessPath = NULL;
		fpSetDllAccessPath = (FPRMBUILDSETDLLACCESSPATH)(::GetProcAddress(m_RmsessionDLL, "SetDLLAccessPath"));		
		// Set the DLL access paths
		if (fpSetDllAccessPath)
		{
			res = (*fpSetDllAccessPath)(m_szDllPath);
		}
		else
		{
			res = HXR_FAIL;
		}
	}

	if (SUCCEEDED(res))
	{		
		FPCREATEJOBFACTORY fpCreateClassFactory = NULL;	
		fpCreateClassFactory = (FPCREATEJOBFACTORY)(::GetProcAddress(m_RmsessionDLL, "HXTCreateJobFactory"));
		// Create the job class factory
		if (fpCreateClassFactory)
		{
			res = (*fpCreateClassFactory)(&m_pFactory);
		}
		else
		{
			res = HXR_FAIL;
		}
	}

	if (SUCCEEDED(res))
	{
		res = m_pFactory->CreateInstance(IID_IHXTLogSystem, (IUnknown**)&m_pLogSystem);
	}

	if (SUCCEEDED(res))
	{
		char szSetTranslationFileDirectory[MAX_PATH];
		wsprintf(szSetTranslationFileDirectory, "%s\\tools", m_pMainFilter->m_params.szSDKPath);
		res = m_pLogSystem->SetTranslationFileDirectory(szSetTranslationFileDirectory);
	}

	if (m_pMainFilter->m_params.bRecordLog)
	{
		if (SUCCEEDED(res))
		{
			res = m_pFactory->CreateInstance(IID_IHXTFileObserver, (IUnknown**)&m_pFileLogObserver);
		}

		if (SUCCEEDED(res))
		{
			res = m_pFileLogObserver->SetFormat(Detailed);
		}

		if (SUCCEEDED(res))
		{
			res = m_pFileLogObserver->Init("c:\\rmwriter.log");
		}
	}

	if (SUCCEEDED(res))
	{
		res = m_pFactory->CreateInstance(IID_IHXTEncodingJob, (IUnknown**)&m_pJob);
	}

	// Register event sink
	if (SUCCEEDED(res))
	{
		// Create event sink helper class
		m_pEventSink = new CSampleEventSink;
		m_pEventSink->AddRef();

		// Get the event manager
		IHXTEventManager* pEventMgr = NULL;
		res = m_pJob->GetEventManager(&pEventMgr);

		// Subscribe for events
		if (SUCCEEDED(res))
		{
			res = pEventMgr->Subscribe(m_pEventSink);
			HX_RELEASE(pEventMgr);
		}
	}

	// Create the property bag used to initialize the input
	IHXTPropertyBag* pInitParams = NULL;
	if (SUCCEEDED(res))
	{
		res = m_pFactory->CreateInstance(IID_IHXTPropertyBag, (IUnknown**)&pInitParams);
	}

	// Set the plugin type
	if (SUCCEEDED(res))
	{
		res = pInitParams->SetString(kPropPluginType, kValuePluginTypeInputMediaSink);
	}

	// Media samples generated in StartEncoding are not strictly real-time
	if (SUCCEEDED(res))
	{
		res = pInitParams->SetBool(kPropIsRealTime, FALSE);
	}

	if (SUCCEEDED(res))
	{
		res = m_pFactory->BuildInstance(IID_IHXTInput, pInitParams, (IUnknown**)&m_pInput);
	}

	// Set the input on the encoding job
	if (SUCCEEDED(res))
	{
		res = m_pJob->SetInput(m_pInput);
	}

	// Create media sample allocator
	if (SUCCEEDED(res))
	{
		res = m_pFactory->CreateInstance(IID_IHXTSampleAllocator, (IUnknown**)&m_pAllocator);
	}

	return res;
}

HRESULT CRMWriter::StartStreaming()
{
	CAutoLock lockit(&m_SyncWriting);

	m_rtAudioTime = 0;
	m_rtVideoTime = 0;

	HX_RESULT res = HXR_OK;

	if (SUCCEEDED(res))
	{
		// Create the output profile
		IHXTOutputProfile* pOutputProfile = NULL;
		if (SUCCEEDED(res))
		{
			res = m_pFactory->BuildInstance(IID_IHXTOutputProfile, NULL, (IUnknown**)&pOutputProfile);
		}

		if (SUCCEEDED(res))
		{
			res = m_pJob->AddOutputProfile(pOutputProfile);
		}

		if (SUCCEEDED(res)
			&& m_pMainFilter->m_params.bUseResize
			&& (m_pMainFilter->m_params.dwResizeWidth > 0)
			&& (m_pMainFilter->m_params.bKeepAspectRatio || m_pMainFilter->m_params.dwResizeHeight > 0))
		{
			// Create the property bag used to initialize the destination
			IHXTPropertyBag* pInitParams = NULL;
			if (SUCCEEDED(res))
			{
				res = m_pFactory->CreateInstance(IID_IHXTPropertyBag, (IUnknown**)&pInitParams);
			}

			// Set the plugin type
			if (SUCCEEDED(res))
			{
				res = pInitParams->SetString(kPropPluginType, kValuePluginTypePrefilterResizer);
			}

			// Set the plugin name
			if (SUCCEEDED(res))
			{
				res = pInitParams->SetString(kPropPluginName, kValuePluginNamePrefilterResizer);
			}

			IHXTPrefilter* pPrefilter = NULL;
			if (SUCCEEDED(res))
			{
				// Create the prefilter
				res = m_pFactory->BuildInstance(IID_IHXTPrefilter, pInitParams, (IUnknown**) &pPrefilter);
			}

			if (SUCCEEDED(res))
			{
				res = pPrefilter->SetUint(kPropVideoResizeWidth, m_pMainFilter->m_params.dwResizeWidth);
				if (SUCCEEDED(res))
				{
					if (m_pMainFilter->m_params.bKeepAspectRatio)
					{
						res = pPrefilter->SetUint(kPropVideoResizeHeight, m_pMainFilter->m_params.dwResizeHeight);
					}
					else
					{
						UINT32 ulHeight = MulDiv(m_pMainFilter->m_params.dwResizeWidth, abs(m_pMainFilter->m_vih.bmiHeader.biHeight), m_pMainFilter->m_vih.bmiHeader.biWidth);
						res = pPrefilter->SetUint(kPropVideoResizeHeight, ulHeight);
					}
				}
				if (SUCCEEDED(res))
				{
					IHXTInput* pInput = NULL;
					res = m_pJob->GetInput(&pInput);
					if (SUCCEEDED(res))
					{
						res = pInput->AddPrefilter(pPrefilter);
					}
					HX_RELEASE(pInput);
				}
			}

			HX_RELEASE(pPrefilter);
			HX_RELEASE(pInitParams);
		}

		if (SUCCEEDED(res))
		{
			// Create the property bag used to initialize the destination
			IHXTPropertyBag* pInitParams = NULL;
			if (SUCCEEDED(res))
			{
				res = m_pFactory->CreateInstance(IID_IHXTPropertyBag, (IUnknown**)&pInitParams);
			}

			// Set the plugin type
			if (SUCCEEDED(res))
			{
				res = pInitParams->SetString(kPropPluginType, kValuePluginTypeDestinationFile);
			}

			// Set the plugin name
			if (SUCCEEDED(res))
			{
				res = pInitParams->SetString(kPropPluginName, kValuePluginNameFileDestRealMedia);
			}

			// Set the pathname
			if (SUCCEEDED(res))
			{
				char szPathAnsi[MAX_PATH];
				WideCharToMultiByte(CP_ACP, 0, m_wszDestFile, wcslen(m_wszDestFile) + 1, szPathAnsi, MAX_PATH, NULL, NULL);
				res = pInitParams->SetString(kPropOutputPathname, szPathAnsi);
				// get its path
				// 01234567
				// c:\12345
				char *p = szPathAnsi + 3;
				for (; *p && *p != '\\'; ++p);
				if (*p != '\0')
				{
					*p = '\0';
				}
				else
				{
					*(szPathAnsi + 3) = '\0';
				}
				res = pInitParams->SetString(kPropTempDirPath, szPathAnsi);
			}

			// Create the destination
			IHXTDestination* pDest = NULL;
			if (SUCCEEDED(res))
			{
				res = m_pFactory->BuildInstance(IID_IHXTDestination, pInitParams, (IUnknown**)&pDest);
			}

			// Set the destination on the output profile
			if (SUCCEEDED(res))
			{
				res = pOutputProfile->AddDestination(pDest);
			}

			HX_RELEASE(pDest);
			HX_RELEASE(pInitParams);
		}

		HX_RELEASE(pOutputProfile);
	}

	if (SUCCEEDED(res))
	{
		// Get the output profile
		IHXTOutputProfile* pOutputProfile = NULL;
		res = m_pJob->GetOutputProfile(0, &pOutputProfile);

		// Create the media profile
		IHXTMediaProfile* pMediaProfile = NULL;
		if (SUCCEEDED(res))
		{
			res = m_pFactory->BuildInstance(IID_IHXTMediaProfile, NULL, (IUnknown**)&pMediaProfile);
		}

		// Set the media profile
		if (SUCCEEDED(res))
		{
			res = pOutputProfile->SetMediaProfile(pMediaProfile);
		}

		UINT32 ulVideo = m_pMainFilter->m_params.dwVideoMode;
		if (SUCCEEDED(res))
		{
			if (ulVideo == 0)
				res = pMediaProfile->SetString(kPropVideoMode, kValueVideoModeNormal);
			else if (ulVideo == 1)
				res = pMediaProfile->SetString(kPropVideoMode, kValueVideoModeSmooth);
			else if (ulVideo == 2)
				res = pMediaProfile->SetString(kPropVideoMode, kValueVideoModeSharp);
			else if (ulVideo == 3)
				res = pMediaProfile->SetString(kPropVideoMode, kValueVideoModeSlideshow);
			else
				res = HXR_FAIL;
		}

		// Set the audio encoding mode
		UINT32 ulAudio = m_pMainFilter->m_params.dwAudioMode;
		if (SUCCEEDED(res))
		{
			if (ulAudio == 0)
				res = pMediaProfile->SetString(kPropAudioMode, kValueAudioModeMusic);
			else if (ulAudio == 1)
				res = pMediaProfile->SetString(kPropAudioMode, kValueAudioModeVoice);
			else
				res = HXR_FAIL;
		}

		HX_RELEASE(pOutputProfile);
		HX_RELEASE(pMediaProfile);
	}

	if (SUCCEEDED(res))
	{
		// Get the output profile
		IHXTOutputProfile* pOutputProfile = NULL;
		res = m_pJob->GetOutputProfile(0, &pOutputProfile);

		// Get the media profile
		IHXTMediaProfile* pMediaProfile = NULL;
		if (SUCCEEDED(res))
		{
			res = pOutputProfile->GetMediaProfile(&pMediaProfile);
		}

		// Create the audience enumerator
		IHXTAudienceEnumerator* pAudienceEnum = NULL;
		if (SUCCEEDED(res))
		{
			res = m_pFactory->CreateInstance(IID_IHXTAudienceEnumerator, (IUnknown**)&pAudienceEnum);
		}

		if (SUCCEEDED(res))
		{
			char szSetProfileDirectory[MAX_PATH];
			wsprintf(szSetProfileDirectory, "%s\\audiences", m_pMainFilter->m_params.szSDKPath);
			res = pAudienceEnum->SetProfileDirectory(szSetProfileDirectory);
		}

		UINT32 ulNumAudiences = 0;
		if (SUCCEEDED(res))
		{
			ulNumAudiences = pAudienceEnum->GetAudienceCount();
		}

		INT32 nAudience = -1;

		const char* szProfileName = (m_pMainFilter->m_params.szProfileName[0] != '\0') ?
			m_pMainFilter->m_params.szProfileName : "750k Download (VBR)";

		// Print all of the available audiences
		for (UINT32 i=0; i<ulNumAudiences && SUCCEEDED(res); i++)
		{
			IHXTAudience* pAudience = NULL;
			res = pAudienceEnum->GetAudience(i, &pAudience, NULL);

			if (SUCCEEDED(res))
			{
				const char* szName = "";
				res = pAudience->GetString(kPropObjectName, &szName);
				if (strcmp(szName, szProfileName) == 0)
				{
					nAudience = i;
					break;
				}
			}
			HX_RELEASE(pAudience);
		}

		if (nAudience == -1)
		{
			res = HXR_FAIL;
		}

		if (SUCCEEDED(res))
		{
			IHXTAudience* pAudience = NULL;
			res = pAudienceEnum->GetAudience((UINT32)nAudience, &pAudience, NULL);

			// Add the audience to the media profile
			if (SUCCEEDED(res))
				res = pMediaProfile->AddAudience(pAudience);

			HX_RELEASE(pAudience);
		}

		// Set some a/v codec properties
		if (SUCCEEDED(res))
		{		
			// Get max startup latency
			UINT32 ulMaxStartupLatency = 4;

			// Get max time between keyframes
			UINT32 ulMaxTimeBetweenKeyframes = 8;

			// Get encoding complexity
			UINT32 ulEncodeComplexity = 1;
			//	"0) Low\n    1) Medium\n    2) High\n? ";

			// Set the values on the appropriate streams within each audience
			UINT32 ulNumAudiences = pMediaProfile->GetAudienceCount();
			for (UINT32 i=0; i<ulNumAudiences && SUCCEEDED(res); i++)
			{
				// Get the audience
				IHXTAudience* pAudience = 0;
				res = pMediaProfile->GetAudience(i, &pAudience);

				if (SUCCEEDED(res))
				{
					// Iterate through all streams within the audience
					UINT32 ulNumStreams = pAudience->GetStreamConfigCount();
					for (UINT32 j=0; j<ulNumStreams && SUCCEEDED(res); j++)
					{
						// Get the stream
						IHXTStreamConfig* pStream = NULL;
						res = pAudience->GetStreamConfig(j, &pStream);

						// Check if the current stream is a video stream
						if (SUCCEEDED(res))
						{
							const char* szStreamType = "";
							res = pStream->GetString(kPropPluginType, &szStreamType);

							// Set max startup latency and max time between keyframes on all video streams
							if (SUCCEEDED(res) && strcmp(szStreamType, kValuePluginTypeVideoStream) == 0)
							{
								res = pStream->SetDouble(kPropMaxStartupLatency, ulMaxStartupLatency);

								if (SUCCEEDED(res))
									res = pStream->SetDouble(kPropMaxTimeBetweenKeyFrames, ulMaxTimeBetweenKeyframes);
							}
						}

						// Set encoding complexity on audio/video streams
						if (SUCCEEDED(res))
						{
							switch (ulEncodeComplexity)
							{
							case 0:
								res = pStream->SetString(kPropEncodingComplexity, kValueEncodingComplexityLow);
								break;
							case 1:
								res = pStream->SetString(kPropEncodingComplexity, kValueEncodingComplexityMedium);
								break;
							default:
							case 2:
								res = pStream->SetString(kPropEncodingComplexity, kValueEncodingComplexityHigh);
								break;
							}
						}

						HX_RELEASE(pStream);
					}
				}

				HX_RELEASE(pAudience);
			}
		}

		HX_RELEASE(pOutputProfile);
		HX_RELEASE(pMediaProfile);
		HX_RELEASE(pAudienceEnum);
	}

	if (SUCCEEDED(res))
	{
		res = m_pJob->SetBool(kPropEnableTwoPass, FALSE);
	}

	IHXTPropertyBag* pMetadata = NULL;
	if (SUCCEEDED(res))
	{
		res = m_pJob->GetMetadata(&pMetadata);
	}

	if (SUCCEEDED(res))
	{
		if (m_pMainFilter->m_params.dwMask & RMWRITER_TITLE)
		{
			if (SUCCEEDED(res))
			{
				res = pMetadata->SetString(kPropTitle, m_pMainFilter->m_params.szTitle);
			}
		}
		if (m_pMainFilter->m_params.dwMask & RMWRITER_AUTHOR)
		{
			if (SUCCEEDED(res))
			{
				res = pMetadata->SetString(kPropAuthor, m_pMainFilter->m_params.szAuthor);
			}
		}
		if (m_pMainFilter->m_params.dwMask & RMWRITER_COPYRIGHT)
		{
			if (SUCCEEDED(res))
			{
				res = pMetadata->SetString(kPropCopyright, m_pMainFilter->m_params.szCopyright);
			}
		}
		if (m_pMainFilter->m_params.dwMask & RMWRITER_DESCRIPTION)
		{
			if (SUCCEEDED(res))
			{
				res = pMetadata->SetString(kPropDescription, m_pMainFilter->m_params.szDescription);
			}
		}
	}

	HX_RELEASE(pMetadata);

	if (SUCCEEDED(res))
	{
		res = m_pJob->StartEncoding(FALSE);
	}

	while (SUCCEEDED(res) && m_pEventSink->m_bEncodingStarted == FALSE)
	{
		Sleep(100);
	}

	return res;
}

HRESULT CRMWriter::StopStreaming()
{
	CAutoLock lockit(&m_SyncWriting);

	HX_RESULT res = HXR_OK;

	if (m_rtVideoTime > 0)
	{ 
		// Create media sample
		IHXTMediaSample* pMediaSample = NULL;
		res = m_pAllocator->GetMediaSampleOfSize(0, &pMediaSample);

		// Mark the sample with ENDOFSTREAM flag
		if (SUCCEEDED(res))
			res = pMediaSample->SetSampleFlags(HXT_SAMPLE_ENDOFSTREAM);

		// Set time
		if (SUCCEEDED(res))
			res = pMediaSample->SetTime(m_rtVideoTime + 1000, m_rtVideoTime + 2000);

		// Encode sample
		if (SUCCEEDED(res))
			res = m_pVideoPin->EncodeSample(pMediaSample);

		HX_RELEASE(pMediaSample);
	}

	// Signal that all audio samples have been sent
	if (m_rtAudioTime > 0)
	{ 
		// Create media sample
		IHXTMediaSample* pMediaSample = NULL;
		res = m_pAllocator->GetMediaSampleOfSize(0, &pMediaSample);

		// Mark the sample with ENDOFSTREAM flag
		if (SUCCEEDED(res))
			res = pMediaSample->SetSampleFlags(HXT_SAMPLE_ENDOFSTREAM);

		// Set time
		if (SUCCEEDED(res))
			res = pMediaSample->SetTime(m_rtAudioTime + 1000, m_rtAudioTime + 2000);

		// Encode sample
		if (SUCCEEDED(res))
			res = m_pAudioPin->EncodeSample(pMediaSample);

		HX_RELEASE(pMediaSample);
	}

	while (SUCCEEDED(res) && m_pEventSink->m_bEncodingStarted && m_pEventSink->m_bEncodingFinished == FALSE)
	{
		Sleep(100);
	}

	// Unsubscribe the event sink -- not needed anymore.  Preserve the StartEncoding() return code.
	if (m_pEventSink)
	{
		// Get the event manager
		IHXTEventManager* pEventMgr = NULL;
		HX_RESULT resEvent = m_pJob->GetEventManager(&pEventMgr);

		// Unsubscribe event sink
		if (SUCCEEDED(resEvent))
			resEvent = pEventMgr->Unsubscribe(m_pEventSink);

		HX_RELEASE(pEventMgr);
		HX_RELEASE(m_pEventSink);
	}

	if (m_pFileLogObserver)
	{
		m_pFileLogObserver->Shutdown();
		HX_RELEASE(m_pFileLogObserver);
	}

	HX_RELEASE(m_pEventSink);
	HX_RELEASE(m_pJob);
	HX_RELEASE(m_pAudioPin);
	HX_RELEASE(m_pVideoPin);

	if (m_pLogSystem)
	{
		m_pLogSystem->Shutdown();
		HX_RELEASE(m_pLogSystem);
	}

	HX_RELEASE(m_pFactory);

	if (m_RmsessionDLL)
	{
		::FreeLibrary(m_RmsessionDLL);
	}

	return NOERROR;
}

// Process audio samples
// Attention: sample's timestamp must be valid!
//  And sync-point, discontinuity...
HRESULT CRMWriter::ReceiveAudio(IMediaSample* pSample)
{
	HRESULT hr = NOERROR;
	REFERENCE_TIME rtStart, rtEnd;
	hr = pSample->GetTime(&rtStart, &rtEnd);
	if (FAILED(hr))
	{
		return hr;
	}

	{
		CAutoLock lockit(&m_SyncWriting);
		m_rtAudioTime = rtStart;
		DbgLog((LOG_TRACE,0,TEXT("Audio sample arrived, time: %I64u"), m_rtAudioTime));

		//REFERENCE_TIME duration = REFERENCE_TIME(rtEnd - rtStart);
		LONG dataSize  = pSample->GetActualDataLength();

		HX_RESULT res = HXR_OK;

		IHXTMediaSample* pMediaSample = NULL;
		res = m_pAllocator->GetMediaSampleOfSize(dataSize, &pMediaSample);

		if (SUCCEEDED(res))
		{
			// Get the sample data buffer -- note use of GetDataStartForWriting instead of 
			// GetDataStartForReading since the buffer is being written to
			UINT32* pSampleBuffer = (UINT32*)pMediaSample->GetDataStartForWriting();
			BYTE* pBuffer;
			pSample->GetPointer(&pBuffer);
			memcpy(pSampleBuffer, pBuffer, dataSize);

			res = pMediaSample->SetTime(rtStart/10000, rtEnd/10000);
		}

		if (SUCCEEDED(res))
			res = m_pAudioPin->EncodeSample(pMediaSample);

		HX_RELEASE(pMediaSample);
	}

	// Try to keep sync
	if (m_pMainFilter->IsVideoPinConnected())
	{
		while (!m_pMainFilter->m_bStopping && m_rtAudioTime - m_rtVideoTime > UNITS)
		{
			Sleep(10);
			DbgLog((LOG_TRACE,0,TEXT("Audio Sleep...")));
		}
	}

	return hr;
}

// Process video samples
// Attention: sample's timestamp must be valid! 
//  And sync-point, discontinuity...
HRESULT CRMWriter::ReceiveVideo(IMediaSample* pSample)
{
	HRESULT hr = NOERROR;
	REFERENCE_TIME rtStart, rtEnd;
	hr = pSample->GetTime(&rtStart, &rtEnd);
	if (FAILED(hr))
	{
		return hr;
	}

	{
		CAutoLock lockit(&m_SyncWriting);
		m_rtVideoTime = rtStart;
		DbgLog((LOG_TRACE,0,TEXT("---Video sample arrived, time: %I64u"), m_rtVideoTime));

		//REFERENCE_TIME duration = REFERENCE_TIME(rtEnd - rtStart);
		LONG dataSize  = pSample->GetActualDataLength();

		HX_RESULT res = HXR_OK;

		IHXTMediaSample* pMediaSample = NULL;
		res = m_pAllocator->GetMediaSampleOfSize(m_dwVideoSampleSize, &pMediaSample);

		if (SUCCEEDED(res))
		{
			// Get the sample data buffer -- note use of GetDataStartForWriting instead of 
			// GetDataStartForReading since the buffer is being written to
			UINT32* pSampleBuffer = (UINT32*)pMediaSample->GetDataStartForWriting();
			BYTE* pBuffer;
			pSample->GetPointer(&pBuffer);
			memcpy(pSampleBuffer, pBuffer, dataSize);

			if (pSample->IsSyncPoint() == S_OK)
			{
				pMediaSample->SetSampleFlags(HXT_SAMPLE_KEYFRAME);
			}
			if (pSample->IsDiscontinuity() == S_OK)
			{
				pMediaSample->SetSampleFlags(HXT_SAMPLE_DATADISCONTINUITY);
			}

			res = pMediaSample->SetTime(rtStart/10000, rtEnd/10000);
		}

		if (SUCCEEDED(res))
			res = m_pVideoPin->EncodeSample(pMediaSample);

		HX_RELEASE(pMediaSample);
	}

	// Try to keep sync
	if (m_pMainFilter->IsAudioPinConnected())
	{
		while (!m_pMainFilter->m_bStopping && m_rtVideoTime - m_rtAudioTime > UNITS)
		{
			Sleep(10);
			DbgLog((LOG_TRACE,0,TEXT("Video Sleep...")));
		}
	}

	return hr;
}
