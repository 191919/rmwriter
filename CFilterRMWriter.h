#pragma once

#include "CRMWriter.h"

class CFilterRMWriter :
	public CBaseFilter,
	public CPersistStream,
	public IRMWriterFilter,
	public ISpecifyPropertyPages,
	public IFileSinkFilter
{
	friend class CXRenderedPin;
	friend class CAudioInputPin;
	friend class CVideoInputPin;
	friend class CRMWriter;

public:
	CFilterRMWriter(LPUNKNOWN lpunk, HRESULT *phr);
	virtual ~CFilterRMWriter();

	static CUnknown * WINAPI CreateInstance(LPUNKNOWN pUnknown, HRESULT * phr);
	DECLARE_IUNKNOWN
	// Basic COM - used here to reveal our own interfaces
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);
	// Pin enumeration
    CBasePin* GetPin(int n);
    int GetPinCount();

	// Open and close the file as necessary
	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();

	HRESULT XEndOfStream();
	HRESULT XCompleteConnect(PIN_ID id);
	HRESULT ReceiveVideo(IMediaSample* pSample);
	HRESULT ReceiveAudio(IMediaSample* pSample);
	void BeginFlush();

	volatile BOOL m_bStopping;
	BOOL IsAudioPinConnected();
	BOOL IsVideoPinConnected();
	CMediaType& GetAudioMediaType();
	CMediaType& GetVideoMediaType();

	// CPersistStream overrides
	HRESULT WriteToStream(IStream* pStream);
	HRESULT ReadFromStream(IStream* pStream);
	int SizeMax();
	STDMETHODIMP GetClassID(CLSID* pClsid);

	// IRMWriterFilter
	STDMETHOD(get_EncodeParameters)(RMWRITER_PARAMS* pParams)
	{
		memcpy(pParams, &m_params, sizeof(m_params));
		return S_OK;
	}

	STDMETHOD(put_EncodeParameters)(RMWRITER_PARAMS* pParams)
	{
		CheckPointer(pParams, E_POINTER);
		if (pParams->cbSize != sizeof(m_params)) return E_INVALIDARG;
		if (pParams->dwMask == (DWORD) -1)
		{
			strcpy(m_params.szSDKPath, pParams->szSDKPath);
			SetSDKPath(m_params.szSDKPath);
		}
		if (pParams->dwMask & RMWRITER_PROFILE)
		{
			strcpy(m_params.szProfileName, pParams->szProfileName);
		}
		if (pParams->dwMask & RMWRITER_RESIZE)
		{
			m_params.bUseResize = pParams->bUseResize;
			m_params.bKeepAspectRatio = pParams->bKeepAspectRatio;
			m_params.dwResizeWidth = pParams->dwResizeWidth;
			m_params.dwResizeHeight = pParams->dwResizeHeight;
		}
		if (pParams->dwMask & RMWRITER_VIDEOMODE)
		{
			m_params.dwVideoMode = pParams->dwVideoMode;
		}
		if (pParams->dwMask & RMWRITER_AUDIOMODE)
		{
			m_params.dwAudioMode = pParams->dwAudioMode;
		}
		if (pParams->dwMask & RMWRITER_TITLE)
		{
			strcpy(m_params.szTitle, pParams->szTitle);
		}
		if (pParams->dwMask & RMWRITER_AUTHOR)
		{
			strcpy(m_params.szAuthor, pParams->szAuthor);
		}
		if (pParams->dwMask & RMWRITER_COPYRIGHT)
		{
			strcpy(m_params.szCopyright, pParams->szCopyright);
		}
		if (pParams->dwMask & RMWRITER_DESCRIPTION)
		{
			strcpy(m_params.szDescription, pParams->szDescription);
		}
		return S_OK;
	}

	// IFileSinkFilter methods
    STDMETHODIMP SetFileName(LPCOLESTR pszFileName, const AM_MEDIA_TYPE *pmt);
    STDMETHODIMP GetCurFile(LPOLESTR * ppszFileName, AM_MEDIA_TYPE *pmt);

	// ISpecifyPropertyPages
	STDMETHODIMP GetPages(CAUUID* pPages);

private:
	CCritSec m_SyncFilter;
	ISeekingPassThru* m_pSeekingPosition; 
	CAudioInputPin* m_pAudioPin;
	CVideoInputPin* m_pVideoPin;
	CRMWriter* m_pRMWriter;
	LONGLONG m_rtCurrent;
	RMWRITER_PARAMS m_params;
	VIDEOINFOHEADER m_vih;
};
