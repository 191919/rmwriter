#pragma once

//
// Have to put those headers here to make them included after <initguid.h>
//

#include "ihxpckts.h"
#include "ihxtencodingjob.h"
#include "ihxtlogsystem.h"
#include "ihxtfileobserver.h"

#define REGPATH_SDKPATH "Software\\RMEncoderFilter"
#define REGKEY_SDKPATH "SDKPath"

DEFINE_GUID(CLSID_RMWriter, 0x3615c0de, 0x0123, 0x4567, 0x89, 0xab, 0xe0, 0x18, 0xcd, 0x32, 0xf3, 0x27);
DEFINE_GUID(IID_IRMWriterFilter, 0x3615c0de, 0x0123, 0x4567, 0x89, 0xab, 0xcd, 0x18, 0xcd, 0x32, 0xf3, 0x27);
DEFINE_GUID(CLSID_RMWriterProp, 0x3615c0de, 0x0123, 0x4567, 0x89, 0xab, 0xcd, 0xef, 0xcd, 0x32, 0xf3, 0x27);

#ifndef SAFE_RELEASE
#define SAFE_RELEASE( x )  \
    if (NULL != x)      \
    {                   \
        x->Release();   \
        x = NULL;       \
    }
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE( x )  \
    if (NULL != x)      \
    {                   \
        delete x;       \
        x = NULL;       \
    }
#endif

typedef enum
{
	PIN_Video = 1,
	PIN_Audio
} PIN_ID;
