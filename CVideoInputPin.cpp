#include "stdafx.h"
#include "RMWriterProp.h"
#include "CVideoInputPin.h"
#include "CFilterRMWriter.h"

CVideoInputPin::CVideoInputPin(CFilterRMWriter* pFilter, HRESULT* phr, LPCWSTR pPinName) :
	CXRenderedPin(pFilter, phr, pPinName, NAME("Video Input"))
{
}

CVideoInputPin::~CVideoInputPin()
{
}

STDMETHODIMP CVideoInputPin::BeginFlush()
{
	m_pMainFilter->BeginFlush();
	return CXRenderedPin::BeginFlush();
}

HRESULT CVideoInputPin::ReceiveXSample(IMediaSample* pSample)
{
	return m_pMainFilter->ReceiveVideo(pSample);
}

HRESULT CVideoInputPin::CheckMediaType(const CMediaType* inMediaType)
{
	CheckPointer(inMediaType, E_POINTER);

	VIDEOINFOHEADER vihDummy = { 0 };
	int n = DSVideoTypeToHXTFormat(inMediaType->subtype, &vihDummy);

	if (inMediaType->majortype == MEDIATYPE_Video &&
		(inMediaType->formattype == FORMAT_VideoInfo || inMediaType->formattype == FORMAT_VideoInfo2) &&
		(n >= 0))
	{
		return S_OK;
	}
	return E_FAIL;
}

HRESULT CVideoInputPin::CompleteConnect(IPin* pReceivePin)
{
	HRESULT hr = m_pMainFilter->XCompleteConnect(PIN_Video);
	if (FAILED(hr)) 
	{
		return hr;
	}

	return CRenderedInputPin::CompleteConnect(pReceivePin);
}
