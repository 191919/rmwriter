#include "stdafx.h"
#include <initguid.h>
#include "defines.h"
#include "RMWriterProp.h"
#include "CFilterRMWriter.h"
#include "CAudioInputPin.h"
#include "CVideoInputPin.h"

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,         // Major CLSID
    &MEDIASUBTYPE_NULL       // Minor type
};

const AMOVIESETUP_PIN psudPins[] =
{
	{
		L"Input",			// Pin's string name
		FALSE,				// Is it rendered
		FALSE,				// Is it an output
		FALSE,				// Allowed none
		FALSE,				// Allowed many
		&CLSID_NULL,		// Connects to filter
		L"Output",			// Connects to pin
		1,					// Number of types
		&sudPinTypes		// Pin information
	},
	{
		L"Input",           // Pin's string name
		FALSE,              // Is it rendered
		FALSE,				// Is it an output
		FALSE,				// Allowed none
		FALSE,				// Allowed many
		&CLSID_NULL,		// Connects to filter
		L"Output",			// Connects to pin
		1,					// Number of types
		&sudPinTypes		// Pin information
	}
};
  
const AMOVIESETUP_FILTER sudflt =
{
    &CLSID_RMWriter,		// CLSID
    L"6rooms RM Encoder",	// Filter's name
    MERIT_DO_NOT_USE,		// Filter's merit
	2,
    psudPins				// Pin information
};


CFactoryTemplate g_Templates [] = 
{
    { 
		L"6.cn RM Encoder",
		&CLSID_RMWriter,
		CFilterRMWriter::CreateInstance,
		NULL,
		&sudflt 
	},
	{
		L"6.cn RM Encoder Property Page",
		&CLSID_RMWriterProp,
		CRMWriterProp::CreateInstance,
		NULL,
		NULL
	}

};

int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);

////////////////////////////////////////////////////////////////////////////

CFilterRMWriter::CFilterRMWriter(LPUNKNOWN lpunk, HRESULT *phr) :
	CBaseFilter(NAME("6rooms RM encoder"), lpunk, &m_SyncFilter, CLSID_RMWriter),
	CPersistStream(lpunk, phr),
	m_pVideoPin(NULL),
	m_pAudioPin(NULL),
	m_rtCurrent(0),
	m_pSeekingPosition(NULL),
	m_bStopping(FALSE)
{
	memset(&m_params, 0, sizeof(m_params));
#ifdef _DEBUG
	m_params.bRecordLog = 1;
#endif
	GetSDKPath(m_params.szSDKPath);

	m_pVideoPin = new CVideoInputPin(this, phr, L"Video In");
	m_pAudioPin = new CAudioInputPin(this, phr, L"Audio In");

	HRESULT hr = NOERROR;
	m_pRMWriter = new CRMWriter(this, &hr);
	if (FAILED(hr))
	{
		*phr = hr;
		return;
	}
	*phr = (m_pVideoPin && m_pAudioPin && m_pRMWriter) ? S_OK : E_OUTOFMEMORY;
}

CFilterRMWriter::~CFilterRMWriter()
{	
	SAFE_RELEASE(m_pSeekingPosition);
	SAFE_DELETE(m_pVideoPin);
	SAFE_DELETE(m_pAudioPin);
	SAFE_DELETE(m_pRMWriter);
}

CUnknown* WINAPI CFilterRMWriter::CreateInstance(LPUNKNOWN pUnknown, HRESULT *phr)
{
	CFilterRMWriter *pFilter = new CFilterRMWriter(pUnknown, phr);
	if (pFilter == NULL)
	{
		*phr = E_OUTOFMEMORY;
	}
	return pFilter;
}

STDMETHODIMP CFilterRMWriter::NonDelegatingQueryInterface(REFIID riid, void** ppv)
{
	CheckPointer(ppv, E_POINTER);

	if (riid == IID_IMediaPosition || riid == IID_IMediaSeeking) 
	{
		// Expose IMediaSeeking interface on our renderer
		HRESULT hr = S_OK;
		if (m_pSeekingPosition == NULL) 
		{
			hr = CreatePosPassThru((IUnknown*) GetOwner(), FALSE, m_pVideoPin, (IUnknown**) &m_pSeekingPosition);
		}
		if (FAILED(hr)) 
		{
			m_pSeekingPosition = NULL;
			return hr;
		}
		return m_pSeekingPosition->QueryInterface(riid, ppv);
	}
	else if (riid == IID_IPersistStream)
	{
		return GetInterface((IPersistStream*) this, ppv);
	}
	else if (riid == IID_ISpecifyPropertyPages)
	{
		return GetInterface((ISpecifyPropertyPages*) this, ppv);
	}
	else if (riid == IID_IRMWriterFilter)
	{
		return GetInterface((IRMWriterFilter*) this, ppv);
	}
	else if (riid == IID_IFileSinkFilter)
	{
		return GetInterface((IFileSinkFilter*) this, ppv);
	}
	return CBaseFilter::NonDelegatingQueryInterface(riid , ppv);
}

void CFilterRMWriter::BeginFlush()
{
	m_bStopping = TRUE;
}

STDMETHODIMP CFilterRMWriter::Run(REFERENCE_TIME tStart)
{
	CAutoLock lck(&m_SyncFilter);
	return CBaseFilter::Run(tStart);
}

STDMETHODIMP CFilterRMWriter::Pause()
{
	CAutoLock lck(&m_SyncFilter);

	// Both input pins should be connected!
	if (m_pAudioPin->IsConnected() == FALSE || m_pVideoPin->IsConnected() == FALSE)
	{
		return E_UNEXPECTED;
	}

	HRESULT hr = NOERROR;
	// Initializing when state from stopped to paused
	if (State_Stopped == m_State)
	{
		m_bStopping = FALSE;
		hr = m_pRMWriter->StartStreaming();
		if (FAILED(hr))
		{
			return hr;
		}
	}

	return CBaseFilter::Pause();
}

STDMETHODIMP CFilterRMWriter::Stop()
{
	m_bStopping = TRUE;

	CAutoLock lck(&m_SyncFilter);
	m_pRMWriter->StopStreaming(); 
	return CBaseFilter::Stop();
}

CBasePin* CFilterRMWriter::GetPin(int n)
{
	if (n == 0)
	{
		return m_pVideoPin;
	}	
	else if (n == 1)
	{
		return m_pAudioPin;
	}
	else
	{
		return NULL;
	}
}

int CFilterRMWriter::GetPinCount()
{
	return 2;
}

HRESULT CFilterRMWriter::XEndOfStream()
{
	BOOL shouldHandleEOS = FALSE;
	if ((m_pAudioPin->IsConnected() && m_pAudioPin->IsEOSReceived()) || !m_pAudioPin->IsConnected())
	{
		if ((m_pVideoPin->IsConnected() && m_pVideoPin->IsEOSReceived()) || !m_pVideoPin->IsConnected())
		{
			shouldHandleEOS = TRUE;
		}
	}
	
	if (shouldHandleEOS)
	{
		if (!m_pAudioPin->m_bCompleteNotified && !m_pVideoPin->m_bCompleteNotified)
		{
			NotifyEvent(EC_COMPLETE, S_OK, (LONG_PTR)(IBaseFilter*) this);
			m_pAudioPin->AfterCompleteHandling();
			m_pVideoPin->AfterCompleteHandling();
		}
	}

	return S_OK;
}

#ifdef _DEBUG
void DisplayMediaType(const CMediaType & mt)
{
	// Dump the GUID types and a short description
	DbgLog((LOG_TRACE, 0, TEXT("")));
	DbgLog((LOG_TRACE, 0, TEXT("Media Type Description")));
	DbgLog((LOG_TRACE, 0, TEXT("Major type: %s"),GuidNames[*mt.Type()]));
	DbgLog((LOG_TRACE, 0, TEXT("Subtype: %s"),GuidNames[*mt.Subtype()]));
	DbgLog((LOG_TRACE, 0, TEXT("Subtype description: %s"),GetSubtypeName(mt.Subtype())));
	DbgLog((LOG_TRACE, 0, TEXT("Format size: %d"),mt.cbFormat));
	// Dump the generic media types
	DbgLog((LOG_TRACE, 0, TEXT("Fixed size sample %d"),mt.IsFixedSize()));
	DbgLog((LOG_TRACE, 0, TEXT("Temporal compression %d"),mt.IsTemporalCompressed()));
	DbgLog((LOG_TRACE, 0, TEXT("Sample size %d"),mt.GetSampleSize()));
}
#endif

HRESULT CFilterRMWriter::XCompleteConnect(PIN_ID id)
{
	HRESULT hr = NOERROR;
	if (PIN_Video == id)
	{
		CMediaType& mt = m_pVideoPin->CurrentMediaType();

		// What's the media type?
#ifdef _DEBUG
		DisplayMediaType(mt);
#endif // _DEBUG

		if (mt.formattype == FORMAT_VideoInfo)
		{
			m_vih = *(VIDEOINFOHEADER*) mt.Format();
		}
		else
		{
			VIDEOINFOHEADER2 vih2 = *(VIDEOINFOHEADER2*) mt.Format();
			m_vih.AvgTimePerFrame = vih2.AvgTimePerFrame;
			m_vih.bmiHeader = vih2.bmiHeader;
			m_vih.dwBitErrorRate = vih2.dwBitErrorRate;
			m_vih.dwBitRate = vih2.dwBitRate;
			m_vih.rcSource = vih2.rcSource;
			m_vih.rcTarget = vih2.rcTarget;
		}

		hr = m_pRMWriter->ConfigVideoInput(mt.subtype, &m_vih);//(VIDEOINFOHEADER*)mt.pbFormat);
	}
	else if (PIN_Audio == id)
	{
		CMediaType& mt = m_pAudioPin->CurrentMediaType();

		// What's the media type?
#ifdef _DEBUG
		DisplayMediaType(mt);
#endif // _DEBUG

		hr = m_pRMWriter->ConfigAudioInput(mt.subtype, (WAVEFORMATEX*)mt.pbFormat);
	}

	return hr;
}

HRESULT CFilterRMWriter::ReceiveVideo(IMediaSample * pSample)
{
	if (m_bStopping) return S_FALSE;

	HRESULT hr = m_pRMWriter->ReceiveVideo(pSample);

	REFERENCE_TIME rtDummy;
	pSample->GetMediaTime(&m_rtCurrent, &rtDummy);
	return hr;
}

HRESULT CFilterRMWriter::ReceiveAudio(IMediaSample * pSample)
{
	if (m_bStopping) return S_FALSE;

	HRESULT hr = S_OK;
	hr = m_pRMWriter->ReceiveAudio(pSample);
	return hr;
}

BOOL CFilterRMWriter::IsAudioPinConnected()
{
	return m_pAudioPin->IsConnected();
}

BOOL CFilterRMWriter::IsVideoPinConnected()
{
	return m_pVideoPin->IsConnected();
}

CMediaType& CFilterRMWriter::GetAudioMediaType()
{
	return m_pAudioPin->CurrentMediaType();
}

CMediaType& CFilterRMWriter::GetVideoMediaType()
{
	return m_pVideoPin->CurrentMediaType();
}

// --- IFileSinkFilter methods ---
STDMETHODIMP CFilterRMWriter::SetFileName(LPCOLESTR pszFileName, const AM_MEDIA_TYPE* pmt)
{
	// Is this a valid filename supplied
	CheckPointer(pszFileName, E_POINTER);
	if (wcslen(pszFileName) > MAX_PATH)
	{
		return ERROR_FILENAME_EXCED_RANGE;
	}
	return m_pRMWriter->SetDestFile(pszFileName);
}

STDMETHODIMP CFilterRMWriter::GetCurFile(LPOLESTR* ppszFileName, AM_MEDIA_TYPE* pmt)
{
	CheckPointer(ppszFileName, E_POINTER);
	*ppszFileName = NULL;

	if (pmt) 
	{
		ZeroMemory(pmt, sizeof(*pmt));
		pmt->majortype = MEDIATYPE_NULL;
		pmt->subtype = MEDIASUBTYPE_NULL;
	}
	return m_pRMWriter->GetDestFile(ppszFileName);
}

///////////////////////////////////////
// CPersistStream

HRESULT CFilterRMWriter::WriteToStream(IStream* pStream)
{
	HRESULT hr;
	ULONG ul = 0;
	hr = pStream->Write((LPVOID) &m_params, sizeof(m_params), &ul);
	return hr;
}

HRESULT CFilterRMWriter::ReadFromStream(IStream* pStream)
{
	HRESULT hr;
	ULONG ul = 0;
	hr = pStream->Read((LPVOID) &m_params, sizeof(m_params), &ul);
	return hr;
}

int CFilterRMWriter::SizeMax()
{
	return sizeof(m_params);
}

STDMETHODIMP CFilterRMWriter::GetClassID(CLSID* pClsid)
{
	CheckPointer(pClsid, E_POINTER);

	*pClsid = CLSID_RMWriter;
	return NOERROR;
}

///////////////////////////////////////
// IPropertypages

STDMETHODIMP CFilterRMWriter::GetPages(CAUUID* pPages)
{
	CheckPointer(pPages, E_POINTER);

	pPages->cElems = 1;
	pPages->pElems = (GUID*) CoTaskMemAlloc(sizeof(GUID));
	if (pPages->pElems == NULL)
	{
		return E_OUTOFMEMORY;
	}

	*(pPages->pElems) = CLSID_RMWriterProp;
	return NOERROR;
}

//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

//
// DllRegisterServer
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2(TRUE);
}

//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);
}
