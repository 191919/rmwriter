#pragma once

class CXRenderedPin : public CRenderedInputPin
{
	friend class CFilterRMWriter;

public:
	CXRenderedPin(CFilterRMWriter *pFilter, HRESULT *phr, LPCWSTR pPinName, TCHAR *pObjectName);
	virtual ~CXRenderedPin();
	
	// Do something with this media sample
    STDMETHODIMP Receive(IMediaSample* pSample);
    STDMETHODIMP EndOfStream();
	STDMETHODIMP BeginFlush();
    STDMETHODIMP EndFlush();
	STDMETHODIMP ReceiveCanBlock();
	// Notify of Run() from filter
	virtual HRESULT Run(REFERENCE_TIME tStart);	

	BOOL IsEOSReceived();
	HRESULT AfterCompleteHandling();
	virtual HRESULT ReceiveXSample(IMediaSample* pSample);

    CMediaType& CurrentMediaType()
	{
		return m_mt;
	}

protected:
	CFilterRMWriter* m_pMainFilter;
	CCritSec m_SyncRecv;	
};
