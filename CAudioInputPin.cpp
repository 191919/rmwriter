#include "stdafx.h"
#include "RMWriterProp.h"
#include "CAudioInputPin.h"
#include "CFilterRMWriter.h"

////////////////////////////////////////////////////////////////////////////
CAudioInputPin::CAudioInputPin(CFilterRMWriter* pFilter, HRESULT* phr, LPCWSTR pPinName) :
	CXRenderedPin(pFilter, phr, pPinName, NAME("Audio Input"))
{
}

CAudioInputPin::~CAudioInputPin()
{
}

STDMETHODIMP CAudioInputPin::BeginFlush()
{
	m_pMainFilter->BeginFlush();
	return CXRenderedPin::BeginFlush();
}

HRESULT CAudioInputPin::ReceiveXSample(IMediaSample* pSample)
{
	return m_pMainFilter->ReceiveAudio(pSample);
}

// The Windows Media audio codecs support PCM audio.
HRESULT CAudioInputPin::CheckMediaType(const CMediaType* inMediaType)
{
	CheckPointer(inMediaType,E_POINTER);
	
	if (inMediaType->majortype == MEDIATYPE_Audio &&
		inMediaType->subtype == MEDIASUBTYPE_PCM &&
		inMediaType->formattype == FORMAT_WaveFormatEx)
	{
		return S_OK;
	}
	return E_FAIL;
}

HRESULT CAudioInputPin::CompleteConnect(IPin* pReceivePin)
{
	HRESULT hr = m_pMainFilter->XCompleteConnect(PIN_Audio);
	if (FAILED(hr)) 
	{
		return hr;
	}

	return CRenderedInputPin::CompleteConnect(pReceivePin);
}
