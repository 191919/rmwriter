#include "stdafx.h"
#include "RMWriterProp.h"
#include "CXRenderedPin.h"
#include "CFilterRMWriter.h"

CXRenderedPin::CXRenderedPin(CFilterRMWriter *pFilter, HRESULT *phr, LPCWSTR pPinName, TCHAR *pObjectName) :
	CRenderedInputPin(pObjectName, pFilter, &pFilter->m_SyncFilter, phr, pPinName)
{
	m_pMainFilter = pFilter;
}

CXRenderedPin::~CXRenderedPin()
{
}

STDMETHODIMP CXRenderedPin::Receive(IMediaSample* pSample)
{
	CheckPointer(pSample,E_POINTER);

	CAutoLock lockit(&m_SyncRecv);

	HRESULT hr = CRenderedInputPin::Receive(pSample);
	if (SUCCEEDED(hr))
	{
		hr = ReceiveXSample(pSample);		
	}
	return hr;
}

HRESULT CXRenderedPin::ReceiveXSample(IMediaSample* pSample)
{
	return NOERROR;
}

STDMETHODIMP CXRenderedPin::BeginFlush()
{
	CAutoLock lck(&m_pMainFilter->m_SyncFilter);

	return CRenderedInputPin::BeginFlush();
}

STDMETHODIMP CXRenderedPin::EndFlush()
{
	CAutoLock lck(&m_pMainFilter->m_SyncFilter);

	return CRenderedInputPin::EndFlush();
}

STDMETHODIMP CXRenderedPin::EndOfStream()
{
	CAutoLock lockit(&m_SyncRecv);

	HRESULT hr = CheckStreaming();
	// Let the filter to do EC_COMPLETE handling for rendered pins
	if (S_OK == hr && !m_bAtEndOfStream) 
	{
		m_bAtEndOfStream = TRUE;
		hr = m_pMainFilter->XEndOfStream();
	}
	return hr;
}

BOOL CXRenderedPin::IsEOSReceived()
{
	return m_bAtEndOfStream;
}

HRESULT CXRenderedPin::AfterCompleteHandling()
{
	m_bCompleteNotified = TRUE;
	return S_OK;
}

// Notify of Run() from filter
HRESULT CXRenderedPin::Run(REFERENCE_TIME tStart)
{
	UNREFERENCED_PARAMETER(tStart);
	m_bCompleteNotified = FALSE;
	if (m_bAtEndOfStream) 
	{
		m_pMainFilter->XEndOfStream();
	}
	return S_OK;
}

STDMETHODIMP CXRenderedPin::ReceiveCanBlock()
{
	return S_OK;
}
