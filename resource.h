//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FilterRMWriter.rc
//
#define IDD_PARAMS                      101
#define IDS_FILTERNAME                  102
#define IDC_SDKPATH                     1001
#define IDC_PROFILEPATH                 1002
#define IDC_USERESIZE                   1003
#define IDC_RESIZEX                     1004
#define IDC_RESIZEY                     1005
#define IDC_KEEPASPECTRATIO             1006
#define IDC_VIDEOMODE                   1007
#define IDC_AUDIOMODE                   1008
#define IDC_TITLE                       1009
#define IDC_AUTHOR                      1010
#define IDC_COPYRIGHT                   1011
#define IDC_DESCRIPTION                 1012
#define IDC_SAVELOG                     1013
#define IDC_RECORDLOG                   1013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
