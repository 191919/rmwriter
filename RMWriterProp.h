#pragma once

#pragma warning(disable: 4514)

struct IRMWriterFilter;

class CFilterRMWriter;

class CRMWriterProp : public CBasePropertyPage
{
	IRMWriterFilter* m_pMainFilter;
	RMWRITER_PARAMS m_params;
	HWND m_hDlg;

public:
	static CUnknown* WINAPI CreateInstance(LPUNKNOWN lpunk, HRESULT* phr);

	// Overrides from CBasePropertyPage
	HRESULT OnConnect(IUnknown* punk);
	HRESULT OnDisconnect(void);
	HRESULT OnDeactivate(void);
	HRESULT OnApplyChanges();

	CRMWriterProp(LPUNKNOWN lpunk, HRESULT *phr);
	~CRMWriterProp()
	{
	}

private:
	BOOL OnReceiveMessage(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
};

BOOL GetSDKPath(char* szPath);
BOOL SetSDKPath(const char* szPath);
