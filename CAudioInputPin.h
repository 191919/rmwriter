#pragma once

#include "CXRenderedPin.h"

class CAudioInputPin : public CXRenderedPin
{
public:
	CAudioInputPin(CFilterRMWriter* pFilter, HRESULT* phr, LPCWSTR pPinName);
	virtual ~CAudioInputPin();
	
	virtual HRESULT ReceiveXSample(IMediaSample* pSample);

	STDMETHODIMP BeginFlush();

    // Check if the pin can support this specific proposed type and format
    HRESULT CheckMediaType(const CMediaType* pmt);
	HRESULT CompleteConnect(IPin* pReceivePin);
};
