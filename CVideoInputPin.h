#pragma once

#include "CXRenderedPin.h"

class CVideoInputPin : public CXRenderedPin
{
public:
	CVideoInputPin(CFilterRMWriter* pFilter, HRESULT* phr, LPCWSTR pPinName);
	virtual ~CVideoInputPin();
	
    virtual HRESULT ReceiveXSample(IMediaSample* pSample);

	STDMETHODIMP BeginFlush();

    // Check if the pin can support this specific proposed type and format
    HRESULT CheckMediaType(const CMediaType* pmt);
	HRESULT CompleteConnect(IPin* pReceivePin);
};
