#pragma once

#include "defines.h"

class CFilterRMWriter;
class CSampleEventSink;

class CRMWriter
{
public:
	CRMWriter(CFilterRMWriter* pMainFilter, HRESULT* phr);
	~CRMWriter();

	HRESULT SetDestFile(LPCOLESTR pFile);
	HRESULT GetDestFile(LPOLESTR* ppFile);	
	
	HRESULT ConfigVideoInput(const GUID& guidSubType, VIDEOINFOHEADER* pvih);
	HRESULT ConfigAudioInput(const GUID& guidSubType, WAVEFORMATEX* pwf);

	HRESULT StartStreaming();
	HRESULT StopStreaming();

	HRESULT ReceiveAudio(IMediaSample* pSample);
	HRESULT ReceiveVideo(IMediaSample* pSample);

private:
	HRESULT InitRMWriter();

private:
	CCritSec			m_SyncWriting;
	CFilterRMWriter *	m_pMainFilter;
	WCHAR				m_wszDestFile[MAX_PATH];

	// Current stream time
	volatile REFERENCE_TIME		m_rtAudioTime;
	volatile REFERENCE_TIME		m_rtVideoTime;

	DWORD m_dwVideoSampleSize;

	CSampleEventSink* m_pEventSink;

	IHXTClassFactory* m_pFactory;
	IHXTEncodingJob* m_pJob;
	IHXTFileObserver* m_pFileLogObserver;
	IHXTLogSystem* m_pLogSystem;
	IHXTMediaInputPin* m_pAudioPin;
	IHXTMediaInputPin* m_pVideoPin;
	IHXTSampleAllocator* m_pAllocator;
	IHXTInput* m_pInput;

	char m_szDllPath[MAX_PATH*10];
	HMODULE m_RmsessionDLL;
};

int DSVideoTypeToHXTFormat(const GUID& guidSubType, VIDEOINFOHEADER* pvih);
