#include "stdafx.h"
#include <initguid.h>
#include "RMWriterProp.h"
#include "CFilterRMWriter.h"
#include "defines.h"
#include "resource.h"
#include "IRMWriterFilter.h"

CUnknown* WINAPI CRMWriterProp::CreateInstance(LPUNKNOWN lpunk, HRESULT* phr)
{
	CUnknown *punk = new CRMWriterProp(lpunk, phr);
	if (punk == NULL)
	{
		if (phr) *phr = E_OUTOFMEMORY;
	}

	return punk;
}


CRMWriterProp::CRMWriterProp(LPUNKNOWN lpunk, HRESULT* phr) :
	CBasePropertyPage(NAME("RMWriter Property Page"), lpunk, IDD_PARAMS, IDS_FILTERNAME),
	m_pMainFilter(NULL),
	m_hDlg(NULL)
{
	memset(&m_params, 0, sizeof(m_params));
}

BOOL GetSDKPath(char* szPath)
{
	HKEY hKey;
	if (RegCreateKeyEx(HKEY_CURRENT_USER, REGPATH_SDKPATH, 0, NULL, 0, KEY_READ, NULL, &hKey, NULL) != ERROR_SUCCESS)
	{
		return FALSE;
	}
	DWORD dwType = REG_SZ;
	DWORD dwSize = MAX_PATH;
	RegQueryValueEx(hKey, REGKEY_SDKPATH, NULL, &dwType, (LPBYTE) szPath, &dwSize);
	RegCloseKey(hKey);

	char* p = szPath + strlen(szPath) - 1;
	for (; p > szPath && *p != '\\'; --p);
	if ((p == szPath + strlen(szPath) - 1) && (p != szPath + 2)) *p = '\0';

	return TRUE;
}

BOOL SetSDKPath(const char* szPath)
{
	HKEY hKey;
	if (RegCreateKeyEx(HKEY_CURRENT_USER, REGPATH_SDKPATH, 0, NULL, 0, KEY_WRITE, NULL, &hKey, NULL) != ERROR_SUCCESS)
	{
		return FALSE;
	}
	RegSetValueEx(hKey, REGKEY_SDKPATH, 0, REG_SZ, (LPCBYTE) szPath, strlen(szPath));
	RegCloseKey(hKey);
	return TRUE;
}

//
// OnReceiveMessage
//
// Override CBasePropertyPage method.
// Handle windows messages for the dialog of the property sheet.
//
BOOL CRMWriterProp::OnReceiveMessage(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	WORD wNotify;

	switch (uMsg) 
	{
	case WM_INITDIALOG:
		m_hDlg = hwnd;

		SetDlgItemText(hwnd, IDC_SDKPATH, m_params.szSDKPath);
		SetDlgItemText(hwnd, IDC_PROFILEPATH, m_params.szProfileName);
		SetDlgItemText(hwnd, IDC_TITLE, m_params.szTitle);
		SetDlgItemText(hwnd, IDC_AUTHOR, m_params.szAuthor);
		SetDlgItemText(hwnd, IDC_COPYRIGHT, m_params.szCopyright);
		SetDlgItemText(hwnd, IDC_DESCRIPTION, m_params.szDescription);

		CheckDlgButton(hwnd, IDC_USERESIZE, m_params.bUseResize ? BST_CHECKED : BST_UNCHECKED);
		CheckDlgButton(hwnd, IDC_RECORDLOG, m_params.bRecordLog ? BST_CHECKED : BST_UNCHECKED);
		
		SendDlgItemMessage(hwnd, IDC_VIDEOMODE, CB_ADDSTRING, 0, (LPARAM) "Normal");
		SendDlgItemMessage(hwnd, IDC_VIDEOMODE, CB_ADDSTRING, 0, (LPARAM) "Smooth");
		SendDlgItemMessage(hwnd, IDC_VIDEOMODE, CB_ADDSTRING, 0, (LPARAM) "Sharp");
		SendDlgItemMessage(hwnd, IDC_VIDEOMODE, CB_ADDSTRING, 0, (LPARAM) "Slideshow");

		SendDlgItemMessage(hwnd, IDC_AUDIOMODE, CB_ADDSTRING, 0, (LPARAM) "Music");
		SendDlgItemMessage(hwnd, IDC_AUDIOMODE, CB_ADDSTRING, 0, (LPARAM) "Voice");

		SendDlgItemMessage(hwnd, IDC_VIDEOMODE, CB_SETCURSEL, m_params.dwVideoMode, 0);
		SendDlgItemMessage(hwnd, IDC_AUDIOMODE, CB_SETCURSEL, m_params.dwAudioMode, 0);

		SetDlgItemInt(hwnd, IDC_RESIZEX, m_params.dwResizeWidth, FALSE);
		SetDlgItemInt(hwnd, IDC_RESIZEY, m_params.dwResizeHeight, FALSE);

		return TRUE;

	case WM_COMMAND:
		wNotify = HIWORD(wParam);
		if (wNotify == EN_CHANGE || wNotify == CBN_SELCHANGE || wNotify == BN_CLICKED)
		{
			m_bDirty = TRUE;
			m_pPageSite->OnStatusChange(PROPPAGESTATUS_DIRTY);
		}
		return TRUE;
	}
	
	return FALSE;
}

HRESULT CRMWriterProp::OnApplyChanges()
{
	GetDlgItemText(m_hDlg, IDC_SDKPATH, m_params.szSDKPath, sizeof(m_params.szSDKPath));
	GetDlgItemText(m_hDlg, IDC_PROFILEPATH, m_params.szProfileName, sizeof(m_params.szProfileName));
	GetDlgItemText(m_hDlg, IDC_TITLE, m_params.szTitle, sizeof(m_params.szTitle));
	GetDlgItemText(m_hDlg, IDC_AUTHOR, m_params.szAuthor, sizeof(m_params.szAuthor));
	GetDlgItemText(m_hDlg, IDC_COPYRIGHT, m_params.szCopyright, sizeof(m_params.szCopyright));
	GetDlgItemText(m_hDlg, IDC_DESCRIPTION, m_params.szDescription, sizeof(m_params.szDescription));

	m_params.dwVideoMode = SendDlgItemMessage(m_hDlg, IDC_VIDEOMODE, CB_GETCURSEL, 0, 0);
	m_params.dwAudioMode = SendDlgItemMessage(m_hDlg, IDC_AUDIOMODE, CB_GETCURSEL, 0, 0);

	m_params.bUseResize = (IsDlgButtonChecked(m_hDlg, IDC_USERESIZE) == BST_CHECKED);
	m_params.bKeepAspectRatio = (IsDlgButtonChecked(m_hDlg, IDC_KEEPASPECTRATIO) == BST_CHECKED);
	m_params.bRecordLog = (IsDlgButtonChecked(m_hDlg, IDC_RECORDLOG) == BST_CHECKED);

	m_params.dwResizeWidth = GetDlgItemInt(m_hDlg, IDC_RESIZEX, NULL, FALSE);
	m_params.dwResizeHeight = GetDlgItemInt(m_hDlg, IDC_RESIZEY, NULL, FALSE);

	SetSDKPath(m_params.szSDKPath);

	m_params.cbSize = sizeof(m_params);
	m_params.dwMask = (DWORD) -1;
	m_pMainFilter->put_EncodeParameters(&m_params);

	return NOERROR;
}

HRESULT CRMWriterProp::OnConnect(IUnknown* punk)
{
	CheckPointer(punk, E_POINTER);

	HRESULT hr = punk->QueryInterface(IID_IRMWriterFilter, (void **) &m_pMainFilter);
	if (FAILED(hr))
	{
		return E_NOINTERFACE;
	}

	m_pMainFilter->get_EncodeParameters(&m_params);
	return NOERROR;
}

HRESULT CRMWriterProp::OnDisconnect()
{
	CheckPointer(m_pMainFilter, E_UNEXPECTED);

	m_pMainFilter->Release();
	m_pMainFilter = NULL;
	return NOERROR;
}

HRESULT CRMWriterProp::OnDeactivate(void)
{
	return NOERROR;
}