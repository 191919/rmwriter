#pragma once

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	DWORD cbSize;
	DWORD dwMask;

	char szSDKPath[MAX_PATH];
	char szProfileName[MAX_PATH];
	char szTitle[256];
	char szAuthor[256];
	char szCopyright[256];
	char szDescription[256];

	DWORD dwResizeWidth;
	DWORD dwResizeHeight;
	DWORD dwVideoMode;
	DWORD dwAudioMode;

	BOOL bRecordLog;
	BOOL bUseResize;
	BOOL bKeepAspectRatio;
} RMWRITER_PARAMS, *PRMWRITER_PARAMS;

#define RMWRITER_PROFILE		(1 << 0)
#define RMWRITER_RESIZE			(1 << 1)
#define RMWRITER_VIDEOMODE		(1 << 2)
#define RMWRITER_AUDIOMODE		(1 << 3)
#define RMWRITER_TITLE			(1 << 4)
#define RMWRITER_AUTHOR			(1 << 5)
#define RMWRITER_COPYRIGHT		(1 << 6)
#define RMWRITER_DESCRIPTION	(1 << 7)
#define RMWRITER_RECORDLOG		(1 << 8)

DECLARE_INTERFACE_(IRMWriterFilter, IUnknown)
{
	STDMETHOD(get_EncodeParameters) (THIS_
		RMWRITER_PARAMS* pParams
		) PURE;
	STDMETHOD(put_EncodeParameters) (THIS_
		RMWRITER_PARAMS* pParams
	) PURE;
};

#ifdef __cplusplus
}
#endif
